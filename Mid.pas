unit Mid;

interface
uses Windows,Messages,MMSystem,Sysutils,Struct;


type

 ThreadStruct=record
  Count:integer;
 end;

 midi_header_t = packed record
	header_size:longword;			          // ������ ��������� � ������ Size of header in bytes
 	file_format:word;			              // MIDI-��� ����� (0, 1, ��� 2) MIDI file type (0, 1, or 2)
	trackscount:word;		              	// ���������� ������ � MIDI-���� Number of tracks in MIDI file
	num_ticks:word;			                // ���������� ������ � �������� ���� (������������ ��� ���������� �����) Number of ticks per quarter note (used to calc tempo)
  tick_length:Word;
 end;

TMidi=class
public

Constructor Create();
Destructor Destroy();
function load_midi(filename:string):boolean;
function GetNameTrack(n:integer):string;
function GetCopyrightNotice(n:integer):string;
function GetInstrumentName(n:integer):string;
Function GetTH:track_header_array_t;
Function GetNumTracks:word;
Function GetTickLenght:word;
Function GetHMidi:HWND;
Function PlayTracks:boolean;
Function StopTracks:boolean;
Function PauseTracks:boolean;
procedure SetEdabled(s:boolean; i:integer);
///////////////////////////////
protected
procedure nulled;
function  MAKEBYTE(a,b:byte):byte;
function  read_vlq_mem(var th:track_header_t;var Status:boolean):longword;
function  read_byte_mem(var th:track_header_t;var Status:boolean):byte;
procedure set_tempo(new_tempo:integer);
function  read_bytes_mem(var th:track_header_t; buf:PChar; num:integer;var Status:boolean):integer;
function  read_text(var th:track_header_t; var buf:String; num:integer;var Status:boolean):integer;
function  analyze:boolean;
function  process_midi_event(var th:track_header_t):boolean;
////////////////////////////////
private
mh:midi_header_t;
th:track_header_array_t;
hmidi:HWND;
thredcount:integer;
CT:ThreadStruct;
ThreadC:boolean;
end;

implementation
uses PThread,classes;
var
	infile:file;
  last_tempo:integer;
  Thread:array of TPlayThread;

Constructor TMidi.Create();
begin
inherited;
midiOutOpen(@hmidi, 0, 0, 0, 0);
thredcount:=0;
end;

Destructor TMidi.Destroy;
begin
midiOutClose(hmidi);
inherited;
end;

Function TMidi.GetNumTracks:word;
begin
  result:=mh.trackscount
end;

Function TMidi.GetTH:track_header_array_t;
begin
  result:=th;
end;

Function TMidi.GetTickLenght:Word;
begin
result:=mh.tick_length;
end;

function IntelWord(Wrd:word):word;
asm
xchg al,ah
end;

function IntelDWord(DWrd:dword):dword;
asm
xchg al,ah
ror eax,16
xchg al,ah
end;
//������ �� 2 ����
function read_short:word;
var
  v:word;
begin
	BlockRead(infile,v,2);
	Result := IntelWord(v)
end;
//������ �� 4 ����
function read_int:longword;
var
  v:longword;
begin
	BlockRead(infile,v,4);
	Result := IntelDWord(v)
end;

function TMidi.MAKEBYTE(a,b:byte):byte;
begin
Result := (a and 15) or (b shl 4)
end;

function TMidi.read_bytes_mem(var th:track_header_t; buf:PChar; num:integer; var Status:boolean):integer;
var i:integer;
begin
  if longword(th.dataptr) - longword(th.data) + longword(num) > th.length then
    begin
      Status:=false;
      exit;
	  end;
  move(th.dataptr^,buf^,num);
	inc(integer(th.dataptr),num);
  Status := true;
	Result := num;
end;

function TMidi.read_text(var th: track_header_t; var buf: String; num: integer; var Status: boolean): integer;
var
    i:integer;
    symbol:Char;
begin
  if longword(th.dataptr) - longword(th.data) + longword(num) > th.length then
    begin
      Status:=false;
      exit;
	  end;
 // GetMem(buf,num + 1);
 // zeromemory(buf,num + 1);
 // buf[num] := #0;

  for i:=0 to num-1 do
    begin
      symbol:=Char(th.dataptr[i]);
      buf:=buf+symbol;
    end;
  //move(th.dataptr^,buf^,num);
	inc(integer(th.dataptr),num);
  Status := true;
	Result := num;
 // FreeMem(buf);
end;

function TMidi.read_vlq_mem(var th:track_header_t;var Status:boolean):longword;     //���������� ���������� ������
var
  value:longword;
  c:byte;
  p:PByteArray;
  i:integer;
begin
	p := th.dataptr;
  if longword(p) - longword(th.data) >= th.length then
    begin
      Status:=false;
      exit;
	  end;
  i := 3;
  value := p^[0];
  inc(integer(p));
  if (value and $80) <> 0 then
    begin
      value := value and $7f;
      repeat
        if longword(p) - longword(th.data) >= th.length then
          begin
  	        //MessageBox(hwndApp, PChar('���� ��������: ' + ms.filename), ' ������ � ������� read_vlq_mem ', MB_ICONERROR);
            Status:=false;
            exit;
	        end;
        c := p^[0];
        inc(integer(p));
    		value := (value shl 7) + (c and $7f);
        dec(i);
      until (c and $80 = 0)// or (i = 0)
    end;
	th.dataptr := p;
  if i < 0 then //Error !!!
    begin
  	  //MessageBox(hwndApp, PChar('���� ��������: ' + ms.filename), ' ������ � ������� read_vlq_mem > 4 bytes', MB_ICONERROR);
     Status:=false;
     exit;// halt(1);
     value := value and $7F;
	  end;
  Status:=true;
  Result := value;
end;

procedure TMidi.SetEdabled(s:boolean; i:integer);
begin
  th[i].enabled:=s;
end;

procedure TMidi.nulled;
var i:word;
begin
  ThreadC:=false;
  if mh.trackscount <> 0 then   // �������� ����� ����� ����������� MIDI ������
		for i := 0 to mh.trackscount - 1 do  //��������� ��� �����
		  begin
			  th[i].dataptr := nil;
			  if th[i].data <> nil then FreeMem(th[i].data);
        FillChar(th[i],sizeof(track_header_t),0);
		  end;
  mh.trackscount:=0;
  last_tempo := 0;
  FileMode   := 0;
end;

function TMidi.read_byte_mem(var th:track_header_t;var Status:boolean):byte;
begin
  if longword(th.dataptr) - longword(th.data) >= th.length then
    begin
  	  //MessageBox(hwndApp, PChar('���� ��������: ' + ms.filename), '������ � ������� read_byte_mem', MB_ICONERROR);
     exit;
	  end;
  Result := th.dataptr^[0];
  inc(integer(th.dataptr));
end;

procedure TMidi.set_tempo(new_tempo:integer);
begin
	mh.tick_length :=round( new_tempo/mh.num_ticks);//� �������������/((new_tempo / 1000) / mh.num_ticks);
end;

function TMidi.process_midi_event(var th:track_header_t):boolean;
var cmd,cmd2:byte;
    len,i,j,tmptime,id:longword;
    channel:integer;
    Status:boolean;
    text:String;//Pchar;
begin
  i:=0;
//  tmptime:=0;
  cmd2:=0;
  Status:=true;

  while Status do
  begin
    tmptime:=read_vlq_mem(th,Status);
    cmd := read_byte_mem(th,Status);  //������� ������� MIDI ������� Read the MIDI event command
    if (cmd = $FF) then////////////////����-�������/////////////////////////////
      begin
            cmd := read_byte_mem(th,Status);
            len := read_vlq_mem(th,Status);
            case (cmd) of
                $03,$02,$04:begin		//��������� ��������
                      //GetMem(text,len + 1);
                      //zeromemory(text,len + 1);
                      //read_bytes_mem(th, text, len,Status);
                      read_text(th, text, len,Status);
                      //text[len] := #0;
                      case (cmd) of
                        $02: th.CopyrightNotice:=text;
                        $03: th.trackname:=text;      //�������� �����
                        $04: th.InstrumentName:=text;
                      end;
                      //FreeMem(text);
                    end;
                $51:begin // Set tempo
                      if len >= 3 then
                        begin
                          read_bytes_mem(th, @id, 3,Status);
                          set_tempo(IntelDWord(id shl 8));
                        end;
                            {for j := 0 to len - 4 do
                            read_byte_mem(th);}
                     end;
                $2F:begin  //����� �����
                      th.notecount:=i;
                      result:=true;
                      exit;
                    end
            else		//����������� �������                            ////
              if len>0 then   ////////////////��� � ��� ������  //��� ���� ����
                for j := 0 to len - 1 do
                  read_byte_mem(th,Status);
            end;
      end
    else
      begin/////////////////��������� ���������/////////////////////////////////
         channel := cmd and 15;
         case cmd shr 4 of // ����� �� 4 ���� ������ ���� �0 ������ 0�
               $09:begin // Note on
                     th.notes[i].time:=tmptime;
                     th.notes[i].note:=read_byte_mem(th,Status);
                     th.notes[i].velocity:=read_byte_mem(th,Status);
                     th.notes[i].status:=true;
                     inc(i);
                   end;
               $08:begin // Note off
                     th.notes[i].time:=tmptime;
                     th.notes[i].note:=read_byte_mem(th,Status);
                     th.notes[i].velocity:=read_byte_mem(th,Status);
                     th.notes[i].status:=false;
                     inc(i);
                   end;
               $0C, // Program Change
               $0D: read_byte_mem(th,Status);// Channel after-touch
               $0F:begin // System message
                     case channel of
                       $01,
                       $03:read_byte_mem(th,Status);
                       $02: begin //Song position
                              read_byte_mem(th,Status);
                              read_byte_mem(th,Status);
                            end;
                       $00: begin  // SYSEX data  �� ����� � ���� ����� ��� ������ �� F7
                              if (cmd2<>$F7) {or (cmd2<>$07)}then  //????
                                cmd2 := read_byte_mem(th,Status);
                            end;
					             $07: begin // ��� ����� ��� ������� �� �������� ��������� ����?

                            end;
                     end;
                   end;
         else
               begin //����� ��������� ��� ��� ����� ������� � 3 ����� �� ������� ��� ���.
               {
               ���� ���, ��� ��� �� �������! �� �������� ��� ����,
               �� ������� �������� ����, �� ����� ���� ����,
               ��� �� ������ � �� �����. ���� ��� �������� ����� ��� �����;
               � ������ ��� ����� ����, ����� � �� ��������� ��������� �����;
               � �� ����� ��� �� ���������, �� ������ ��� �� ��������.
               ���� ���, ����� �� �������! �� �������� ��� ����; �� �������
               �������� ����;�� ����� ���� ���� � �� �����, ��� �� ����;
               ���� ��� �������� ��� ��� �� ��� ����;� ������ ��� ����� ����,
               ��� � �� ������� ��������� �����; � �� ����� ��� � ���������,
               �� ������ ��� �� ��������. ��� ���� ���� ������� � ���� � ����� �� ����. �����. }
                 read_byte_mem(th,Status);
                 read_byte_mem(th,Status);
               end;
         end;
      end;//////////////////////////////////////////////////////////////////////
  end

end;

function TMidi.analyze:boolean;
var
  i:byte;
begin
	for i := 0 to mh.trackscount-1 do
	  begin
		  th[i].enabled := True;
		  th[i].dataptr := th[i].data;
		  th[i].tracknum := i;
	  end;
  for i := 0 to mh.trackscount-1 do
    if process_midi_event(th[i])=false then
      begin
        Result:=false;
        exit;
      end;
  result:=true;
end;

function TMidi.load_midi(filename:string):boolean;
var
	id:longword;

  LongMTrk:word;
begin
  Result := False;
  Nulled;

  //��������
  AssignFile(infile,filename);
  Reset(infile,1);

  BlockRead(infile, id, 4);
  if id<>MThd then exit;//�� ���� ������

  mh.header_size := read_int;  // Read the header size
  mh.file_format := read_short; // Identify the file format

  if not mh.file_format in [0..2] then exit;//�� ��������� ���

  LongMTrk      := read_short; 	// Output other information present in the header
  mh.num_ticks  := read_short;

  while not eof(infile) do // Read tracks
  begin
    BlockRead(infile, id, 4); // Read the track header
    if id<>MTrk then break;   //�� ���������� �.� �� ���������� � mtrk

    th[mh.trackscount].length := read_int;  // ����� ����� � ���������� ����
    setlength(th[mh.trackscount].notes,th[mh.trackscount].length);
    GetMem(th[mh.trackscount].data, th[mh.trackscount].length);
    BlockRead(infile,th[mh.trackscount].data^,th[mh.trackscount].length);//������ �������� � ������ �����
    inc(mh.trackscount);
    if (mh.trackscount >= MAX_MIDI_TRACKS) then break
  end;
  CloseFile(infile);
  Result:=analyze;
end;

function TMidi.PlayTracks:boolean;
var i:word;
begin
if ThreadC=false then
begin
  SetLength(Thread,mh.trackscount);

  for i:= 0 to mh.trackscount-1 do
    begin
      Thread[i]:=TPlayThread.Create(True);
      Thread[i].Priority:=tpTimeCritical;
      Thread[i].SetMid(mh,th,i,hmidi);
    end;
    ThreadC:=true;
end;

for i:= 0 to mh.trackscount-1 do
  Thread[i].Resume;
end;

function TMidi.PauseTracks:boolean;
var i:word;
begin
if ThreadC then
  for i := 0 to mh.trackscount-1 do
    Thread[i].Suspend;
end;

function TMidi.StopTracks:boolean;
var
  i: Integer;
begin
for i := 0 to mh.trackscount-1 do
   Thread[i].Suspend;
ThreadC:=false;
end;

function TMidi.GetNameTrack(n:integer):string;
begin
  result:=th[n].trackname;
end;

function TMidi.GetCopyrightNotice(n:integer):string;
begin
  result:=th[n].CopyrightNotice;
end;

function TMidi.GetHMidi: HWND;
begin
result:=HMidi;
end;

function TMidi.GetInstrumentName(n:integer):string;
begin
  result:=th[n].InstrumentName;
end;

end.