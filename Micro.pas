unit Micro;

interface
 uses Winapi.Messages,Winapi.MMSystem, Vcl.Controls,WinAPI.windows,Struct;
type
TData = array[0..16384] of smallint;
PData = ^TData;

TMicro=class
public
Constructor  Create;overload;
Constructor  Create(Stereo:boolean;FrequencySample:integer; BitSample:word;Handle:HWND;N:dWord);overload;
//Mono=true 1 ����� =false 2 ������
//FrequencySample:������� ������������� (44100)(22050)
//BitSample:      ����������� 8-16
//Handle:         �������� ����� ����(�� ����!)
//N:              ���������� ��������

Destructor   Destroy; override;//
Procedure    Start;
Procedure    Stop;
Function     GetVol(var LVol: DWORD; var RVol: DWORD): Boolean;
Function     SetVol(LVol,RVol: WORD):Boolean;
Function     OnWaveIn(var Msg: TMessage):BufD;


private
PStart: boolean;
WaveIn: hWaveIn;           //����� ��������� ����� ���������� �����.
hBuf: THandle;             //����� ������ �������
BufHead: TWaveHdr;         //��������� WAVEHDR ���������� ���������, ������������ ��� ������������� ������� ����� ������.
tN:DWORD;
leftcn,rightcn:word;

end;

implementation

Constructor TMicro.Create;
begin
  inherited;
end;

Constructor TMicro.Create(Stereo:boolean;FrequencySample:integer; BitSample:word;Handle:HWND;N:dWord);
var
  FHead: TWaveFormatEx;    //��������� WAVEFORMATEX ���������� ������ �����������
  BufLen: word;            //������ ������ �������
  buf: pointer;
begin
//�������� ��������� ������� ������
    with FHead do begin
//����� ������������ ����-���������� ���������
      wFormatTag := WAVE_FORMAT_PCM;
//����������� ����/������
      nChannels := Integer(Stereo)+1;
//�������������
      nSamplesPerSec := FrequencySample;
//����������� ������
      wBitsPerSample := BitSample;
// ����� ���� � �������
      nBlockAlign := nChannels * (wBitsPerSample div 8);
// ������� �������� �������� ������ ����/���
      nAvgBytesPerSec := nSamplesPerSec * nBlockAlign;
// ��� ������� ��� ������ �� �����, ������� ��������
      cbSize := 0;
    end;

    WaveInOpen(Addr(WaveIn), WAVE_MAPPER, addr(FHead), Handle, 0, CALLBACK_WINDOW);
    BufLen := FHead.nBlockAlign * N;
    hBuf := GlobalAlloc(GMEM_MOVEABLE and GMEM_SHARE, BufLen);
    Buf := GlobalLock(hBuf);

    with BufHead do begin
      lpData := Buf;
      dwBufferLength := BufLen;
      dwFlags:= WHDR_BEGINLOOP
    end;

    tN:=N;

end;

destructor TMicro.Destroy();
begin
  WaveInReset(WaveIn);
  WaveInUnPrepareHeader(WaveIn, Addr(BufHead), sizeof(BufHead));
  WaveInClose(WaveIn);
  GlobalUnlock(hBuf);
  GlobalFree(hBuf);
  //������������ ����������
  inherited;
end;

Procedure TMicro.Start;
begin
PStart:=True;
WaveInPrepareHeader(WaveIn, Addr(BufHead), sizeof(BufHead));
WaveInAddBuffer(WaveIn, Addr(BufHead), sizeof(BufHead));
WaveInStart(WaveIn);
end;

Procedure TMicro.Stop;
begin
PStart:=False;
end;

function TMicro.GetVol(var LVol: DWORD; var RVol: DWORD): Boolean;
var
  WaveOutCaps: TWAVEOUTCAPS;
  Volume: DWORD;
begin
   Result := False;
   if WaveOutGetDevCaps(WAVE_MAPPER, @WaveOutCaps, SizeOf(WaveOutCaps)) = MMSYSERR_NOERROR then
     if WaveOutCaps.dwSupport and WAVECAPS_VOLUME = WAVECAPS_VOLUME then
     begin
       Result := WaveOutGetVolume(WAVE_MAPPER, @Volume) = MMSYSERR_NOERROR;
       leftcn :=LoWord(Volume);
       rightcn:= HiWord(Volume);

       LVol:=leftcn;
       RVol:=rightcn
     end;
end;

Function TMicro.SetVol(LVol,RVol: WORD):Boolean;
var
  WaveOutCaps: TWAVEOUTCAPS;
  AVolume    : DWORD;
begin
   Result := False;
   if WaveOutGetDevCaps(WAVE_MAPPER, @WaveOutCaps, SizeOf(WaveOutCaps)) = MMSYSERR_NOERROR then
     if WaveOutCaps.dwSupport and WAVECAPS_VOLUME = WAVECAPS_VOLUME then
        begin
          AVolume:= MakeLong(LVol, RVol);
          Result := WaveOutSetVolume(WAVE_MAPPER, AVolume) = MMSYSERR_NOERROR;
        end;
end;

Function TMicro.OnWaveIn(var Msg: TMessage):BufD;
var
i: integer;
CapData: PData;               //����� ������� �����
BufData: BufD;                //������������� �����
begin
//������ �����
//  SetLength(^CapData,tN-1);
  CapData := PData(PWaveHdr(Msg.lParam)^.lpData);

//����������� ������ � �����
  SetLength(BufData,tN);
  for i:=0 to tN-1 do
    BufData[i]:=CapData^[i];

//���������� ����� ����� �������
  if PStart=True then
   WaveInAddBuffer(WaveIn, PWaveHdr(Msg.lParam),SizeOf(TWaveHdr))
  else
   PStart:= True;

   result:=BufData;

end;





end.
