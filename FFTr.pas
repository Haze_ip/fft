unit FFTr;

interface
uses cmplx,struct;

type
TFFT=class
private
  FrecencyMicro:word;
  SizeBuf:word;
  SizeBufReal:word;
  Tres:TCmxArray;
  D:BufD;
  function GetFFTExpTable(CountFFTPoints:Integer; InverseFFT:Boolean=False): TCmxArray;// ������� ������ ���������� Wn ���� ������ ��� �� ���������
  function GetArrayIndex(CountPoint: Integer;MinIteration: Integer): TIndexArray;// ���������� �������� ��� ��� ��� ����� ��������� � ������ �������� ���� �� ������������ ��� ������ ��������
  procedure NormalizeFFTSpectr(var FFTArray: TCmxArray);overload;
  procedure NormalizeFFTSpectr(var FFTArray: TCmxArray; Mnogitel: Double);overload;
  procedure _fft(var D:TCmxArray; StartIndex,ALen:Integer;const TableExp:TCmxArray); register;
  Function  Gausse(n, BufSize: Integer):real;
  function FFT(Mnozh:word):TCmxArray;
public
  Constructor Create;overload;
  Constructor Create(SiBuF,FrecMic:word);overload;

  function GetMainFrecensy(DD:bufD;Amplitude:real;CountFrecensy,mnozh:word): MFrAm;
//  Function
end;
implementation


function TFFT.FFT(Mnozh:word):TCmxArray;
var i:word;
    Sam,TableExp:TCmxArray;
    Table4Index:TIndexArray;
    oldBuf:word;
begin


  SizeBUf:=SizeBufReal;
  for i := 0 to SizeBuf-1 do
  begin
//    window:=0.5 - 0.5 * cos(2.0 * pi * i / SizeBuf);
//    TRes[i].x := d[i].x / SizeBuf * window;
      D[i] := D[i]*Gausse(i, SizeBuf);
  end;
  if mnozh<>1 then
    begin
    OldBuF:=SizeBuf;
    SizeBuf:=SizeBufReal*Mnozh;
    Table4Index := GetArrayIndex(SizeBuf,2);  //� ������ ��������� 1 ���
    TableExp    := GetFFTExpTable(SizeBuf);   //
    SetLength(Sam, SizeBuf);
    SetLength(TRes,SizeBuf);
    {for i := 0 to (SizeBuf div 2)-(OldBuf div 2)-1 do
    TRes[i].x:=0; }
    for i := (SizeBuf div 2)-(OldBuf div 2) to (SizeBuf div 2)+(OldBuf div 2)-1 do
    TRes[i].x:=D[i-((SizeBuf div 2)-(OldBuf div 2))];
    {for i := (SizeBuf div 2)+(OldBuf div 2) to SizeBuf -1 do
    TRes[i].x:=0; }
    end
    else
    begin

      Table4Index := GetArrayIndex(SizeBuf,2);
      TableExp    := GetFFTExpTable(SizeBuf);
      SetLength(Sam, SizeBuf);
      SetLength(TRes,SizeBuf);

      for i := 0 to SizeBuf-1 do
      TRes[i].x:=D[i];
    end;

//  TRes[i].x:=D[i];
  for i := 0 to SizeBuf-1 do
     Sam[i]:=TRes[Table4Index[I]];

  for i := 0 to SizeBuf-1 do    //��� ��������������
     TRes[i]:=Sam[i];

 _fft(Tres,0,Length(TRes),TableExp);
 NormalizeFFTSpectr(TRes);

 for i := 0 to SizeBuf-1 do
 begin
     TRes[i].x:=TRes[i].x/512;
     TRes[i].y:=TRes[i].y/512;
 end;
 Result:=TRes;
end;


function TFFT.GetMainFrecensy(DD:bufD;Amplitude:real;CountFrecensy,mnozh:word): MFrAm;
var i,j,a,b:integer;
p:Double;
pik: array of PFrecAmpl;
res:MFrAm;
Shag,t:Double;
begin
D:=DD;
FFT(Mnozh);
SetLength(pik,SizeBuf);
Shag:=FrecencyMicro/sizebuf;
j:=-1;
  for i :=1 to SizeBuf-2 do
   if (TRes[i-1].x<TRes[i].x)and(TRes[i].x>Tres[i+1].x)and(TRes[i].x>Amplitude) then//������ ��� ����
    begin
      if i*shag<22000 then
      begin
        inc(j);
        pik[j].Fr:=i;
        pik[j].Ampl:=TRes[i].x;
      end;
    end;

 //������������� � ������� �������� �� ���������
 for a := 0 to J-1 do
   for b := 0 to J-A-1 do
   if pik[b].Ampl>pik[b+1].Ampl then
    begin //����� ���������
      p:=pik[j].Ampl;
      pik[j].Ampl:=pik[j+1].Ampl;
      pik[j+1].Ampl:=P;
    end;

    SetLength(Res,CountFrecensy);
      for i := 0 to CountFrecensy-1 do
        begin
          Res[i].Fr:=pik[i].Fr*Shag;
          Res[i].Ampl:=pik[i].Ampl;
        end;
      setLength(Result,CountFrecensy);
  Result:=res;
end;

Function TFFT.Gausse(n, BufSize: Integer):real;
var A,t:real;
begin
A:=(BufSize-1)/2;
T:=(n - a)/(QGause*A);
t:= t*t;

result:=exp(-t/2);

end;

function TFFT.GetArrayIndex(CountPoint: Integer; MinIteration: Integer): TIndexArray;
Var I,LenBlock,HalfBlock,HalfBlock_1,
    StartIndex,EndIndex,ChIndex,NChIndex :Integer;
    TempArray:TIndexArray;
begin

  EndIndex:=CountPoint-1;

  SetLength(Result,CountPoint);
    For I:=0 to EndIndex do           //����������� ������� �� �������
      Result[I]:=I;


  LenBlock   :=CountPoint;
  HalfBlock  :=LenBlock shr 1;
  HalfBlock_1:=HalfBlock -1;


  while LenBlock > MinIteration do    //������������ ������� � ������ �������  LenBlock
    begin
      StartIndex:=0;                  //�������� � �������� ������ �����
      TempArray :=Copy(Result,0,CountPoint);

      repeat //������������ ������� � ����������(������ ����� =StartIndex) ����� �������  LenBlock
        ChIndex :=StartIndex;
        NChIndex:=ChIndex+1;
        EndIndex:=StartIndex+HalfBlock_1;

        //�������� ������ � �������� ������� � �������� ������� � �������� ������
        for I:=StartIndex to EndIndex do
          begin
            Result[I]          :=TempArray[ChIndex];
            Result[I+HalfBlock]:=TempArray[NChIndex];

            ChIndex :=ChIndex +2;
            NChIndex:=NChIndex+2;
          end;

        StartIndex:=StartIndex+LenBlock;   //��������� � ������� �����
      Until StartIndex >= CountPoint;      //���� ����� �� ����� �������, ������ ������ ������ ���.

      //��������� ����� ����, ���� �� ������ �� ������������ ������� (MinIteration)
      LenBlock   :=LenBlock shr 1;
      HalfBlock  :=LenBlock shr 1;
      HalfBlock_1:=HalfBlock -  1;
    end;


  TempArray:=Nil;
end;

procedure TFFT.NormalizeFFTSpectr(var FFTArray: TCmxArray);
begin
NormalizeFFTSpectr(FFTArray, 2/Length(FFTArray));
end;

procedure TFFT.NormalizeFFTSpectr(var FFTArray: TCmxArray; Mnogitel: Double);
var I,Len:Integer;
begin
Len:= Length(FFTArray);
Len:= Len shr 1;  //Len := Len/2;
Dec(Len);  //�.� ���������� � ����
For I:=0 to Len do  begin
  FFTArray[I].X:=cAbs(FFTArray[I]);
  FFTArray[I].X:=FFTArray[I].X*Mnogitel;
end;

end;

procedure TFFT._fft(var D: TCmxArray; StartIndex, ALen: Integer;const TableExp:TCmxArray);
var
I,NChIndex,TableExpIndex:Integer;
TempBn,D0,D1:TComplex;
begin
if ALen=2 then //�������� ����������� ��������
  begin
    D0  :=D[StartIndex];
    D1  :=D[StartIndex+1];
    D[StartIndex]   :=cSum(D0,D1);
    D[StartIndex+1] :=cSub(D0,D1);
    exit;
  end;
ALen  := ALen shr 1;
NChIndex:= StartIndex+ALen;
_fft(D,StartIndex,ALen,TableExp); //�������� ���, ��� ������ �������� ������
_fft(D,NChIndex,ALen,TableExp);  //�������� ���, ��� ������ �������� ������
TableExpIndex:=ALen;
Dec(ALen);

for I:=0 to ALen do//����� �������������� ������� ��� (������� ��� ���������)
begin
  TempBn  :=cMul(D[NChIndex],TableExp[TableExpIndex+I]);
  D0            :=D[StartIndex];
  D[StartIndex] :=cSum(D0,TempBn);
  D[NChIndex]   :=cSub(D0,TempBn);
  Inc(NChIndex);
  Inc(StartIndex);
end;
end;

constructor TFFT.Create;
begin
inherited;
end;

constructor TFFT.Create(SiBuF, FrecMic: word);
begin
//inherited;
SizeBuFReal:=SiBuF;
FrecencyMicro:=FrecMic;
end;

function TFFT.GetFFTExpTable(CountFFTPoints:Integer; InverseFFT:Boolean=False): TCmxArray;
var I,StartIndex,N,LenB:Integer;
    w,wn:TComplex;
    Mnogitel:Double;
begin

  Mnogitel := -2*Pi;               //������  ���

  if InverseFFT then
     Mnogitel:= 2*Pi;              //��������  ���


  SetLength(Result,CountFFTPoints);//��������� ����� ������� ���

  LenB:=1;

  while LenB <CountFFTPoints do //��������� ������ ����.
    begin
      N     := LenB;      //������� ����� ��������� � �����
      LenB  := LenB shl 1;

      //������� ������ EXP ���������(������) ��� ���.
      wn.x := 0;
      wn.y := Mnogitel/LenB;
      wn   := cExp(wn);
      w.x  := 1;

      StartIndex:=N;    //��������� ������ � �������.
      Dec(N);           //�.� ���������� � ����
      For I:=0 to N do  //��������� ���� exp ����������� ��� ���, � �������
        begin
          Result[StartIndex+I]:=w;
          w:=cMul(w,wn);
        end;
    end;
end;




end.
